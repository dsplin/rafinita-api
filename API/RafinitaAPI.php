<?php

class RafinitaAPI
{
    private $apiKey;
    private $apiPassword;
    private $apiUrl;

    /**
     * RafinitaAPI constructor.
     * @param string $apiKey
     * @param string $apiPassword
     * @param string $apiUrl
     */
    public function __construct($apiKey, $apiPassword, $apiUrl)
    {
        $this->apiKey = $apiKey;
        $this->apiPassword = $apiPassword;
        $this->apiUrl = $apiUrl;
    }

    /**
     * Sends a request to the API endpoint.
     *
     * @param array $requestData
     * @return string
     * @throws Exception
     */
    public function sendRequest($requestData)
    {
        $url = $this->apiUrl;

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($requestData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . base64_encode($this->apiKey . ':' . $this->apiPassword)
        ]);

        $response = curl_exec($ch);

        if ($response === false) {
            throw new Exception('Request failed: ' . curl_error($ch));
        }

        curl_close($ch);

        return $response;
    }

    /**
     * Sends a SALE request to the API endpoint.
     *
     * @param array $orderData
     * @return RafinitaAPIResponse
     * @throws Exception
     */
    public function sendSaleRequest($orderData)
    {
        $requestData = array_merge([
            'action' => 'SALE',
            'client_key' => $this->apiKey,
            'hash' => $this->generateHash($orderData['payer_email'], $this->apiPassword, $orderData['card_number']),
        ], $orderData);

        $response = $this->sendRequest($requestData);
        $response = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Invalid JSON response: ' . json_last_error_msg());
        }

        return new RafinitaAPIResponse($response);
    }

    /**
     * Generates a hash value based on email, client password, and card number.
     *
     * @param string $email
     * @param string $clientPass
     * @param string $cardNumber
     * @return string
     */
    public function generateHash($email, $clientPass, $cardNumber)
    {
        $hashString = strtoupper(strrev($email) . $clientPass . strrev(substr($cardNumber, 0, 6) . substr($cardNumber, -4)));
        $hash = md5($hashString);

        return $hash;
    }

    /**
     * Generates a callback hash value based on email, client password, transaction ID, and card number.
     *
     * @param string $email
     * @param string $clientPass
     * @param string $transId
     * @param string $cardNumber
     * @return string
     */
    public function generateCallbackHash($email, $clientPass, $transId, $cardNumber)
    {
        $hashString = strtoupper(strrev($email) . $clientPass . $transId . strrev(substr($cardNumber, 0, 6) . substr($cardNumber, -4)));
        $hash = md5($hashString);

        return $hash;
    }
}
