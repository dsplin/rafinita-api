<?php

class HandlerResponse
{
    /**
     * Sends a success response.
     *
     * @param mixed $result
     */
    public static function success($result)
    {
        $response = [
            'status' => 'success',
            'data' => $result
        ];
        echo json_encode($response);
    }

    /**
     * Sends an error response.
     *
     * @param string $errorMessage
     */
    public static function error($errorMessage)
    {
        $response = [
            'status' => 'error',
            'message' => $errorMessage
        ];
        echo json_encode($response);
    }

    /**
     * Sends a validation errors response.
     *
     * @param array $errors
     */
    public static function validationErrors($errors)
    {
        $response = [
            'status' => 'error',
            'message' => 'Validation errors',
            'errors' => $errors
        ];
        echo json_encode($response);
    }
}
