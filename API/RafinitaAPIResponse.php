<?php

class RafinitaAPIResponse
{
    private $response;

    /**
     * RafinitaAPIResponse constructor.
     * @param array $response
     */
    public function __construct($response)
    {
        $this->response = $response;
    }

    /**
     * Gets the result from the response.
     *
     * @return string
     * @throws Exception
     */
    public function getResult()
    {
        if (!isset($this->response['result'])) {
            throw new Exception('Result not found in response');
        } elseif ($this->response['result'] == 'ERROR') {
            throw new Exception(json_encode($this->response));
        } else {
            return json_encode($this->response);
        }
    }

    /**
     * Gets the status from the response.
     *
     * @return string
     * @throws Exception
     */
    public function getStatus()
    {
        if (!isset($this->response['status'])) {
            throw new Exception('Status not found in response');
        }

        return $this->response['status'];
    }
}
