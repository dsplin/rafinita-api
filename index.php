<?php

require_once 'API/RafinitaAPI.php';
require_once 'API/RafinitaAPIResponse.php';
require_once 'Handlers/HandlerResponse.php';
require_once 'Requests/SaleRequest.php';
$config = require_once 'Config/config.php';

$apiKey = $config['apiKey'];
$apiPassword = $config['apiPassword'];
$apiUrl = $config['apiUrl'];

$rafinitaAPI = new RafinitaAPI($apiKey, $apiPassword, $apiUrl);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $jsonData = file_get_contents('php://input');
    $orderData = json_decode($jsonData, true);

    if ($orderData === null) {
        HandlerResponse::error('Invalid JSON data.');
        exit;
    }

    if (!empty($orderData['action'])) {
        switch ($orderData['action']) {
            case 'SALE':
                $validationErrors = SaleRequest::validate($orderData);

                if (!empty($validationErrors)) {
                    HandlerResponse::validationErrors($validationErrors);
                } else {
                    try {
                        $response = $rafinitaAPI->sendSaleRequest($orderData);
                        HandlerResponse::success($response->getResult());
                    } catch (Exception $e) {
                        HandlerResponse::error($e->getMessage());
                    }
                }
                break;
            case 'CAPTURE':
                HandlerResponse::error('The CAPTURE method is not finished.');
                break;
            case 'CREDITVOID':
                HandlerResponse::error('The CREDITVOID method is not finished.');
                break;
            default:
                HandlerResponse::error('Invalid action.');
                exit;
        }
    } else {
        HandlerResponse::error('Action is required.');
        exit;
    }
} else {
    HandlerResponse::error('Invalid request method. Only POST requests are allowed.');
}