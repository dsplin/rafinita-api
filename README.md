# Rafinita API

## Requirements

Before starting the deployment, make sure your system meets the following requirements:

- PHP

## Deployment

To deploy the project, follow these steps:

1. Clone the repository:
   ```shell
   git clone git@gitlab.com:dsplin/rafinita-api.git
   
2. Navigate to the project directory:
   ```shell
   cd rafinita-api
   
3. Run these commands
   ```shell
   php -S localhost:8000
   
4. Send curl request
   ```shell
   curl --location --request POST 'http://localhost:8000/' \
   --header 'Content-Type: application/json' \
   --data-raw '{
   "action": "SALE",
   "order_id": "012345677788",
   "order_amount": "100.00",
   "order_currency": "USD",
   "order_description": "Test Order",
   "card_number": "4111111111111111",
   "card_exp_month": "12",
   "card_exp_year": "2025",
   "card_cvv2": "123",
   "payer_first_name": "Dima",
   "payer_last_name": "Hr",
   "payer_email": "Dima.Hr@example.com",
   "payer_phone": "1234567890",
   "payer_address": "123 Street",
   "payer_city": "Lutsk",
   "payer_country": "US",
   "payer_zip": "43000",
   "payer_ip": "192.168.0.1",
   "term_url_3ds": "https://example.com/3ds-callback",
   "recurring_init": "N",
   "auth": "N"
   }'
   
5. Or send request with POSTMAN
   ```shell  
   POST http://localhost:8000/ 
   Body (application/json): 
   {
    "action": "SALE",
    "order_id": "012345677788",
    "order_amount": "100.00",
    "order_currency": "USD",
    "order_description": "Test Order",
    "card_number": "4111111111111111",
    "card_exp_month": "12",
    "card_exp_year": "2025",
    "card_cvv2": "123",
    "payer_first_name": "Dima",
    "payer_last_name": "Hr",
    "payer_email": "Dima.Hr@example.com",
    "payer_phone": "1234567890",
    "payer_address": "123 Street",
    "payer_city": "Lutsk",
    "payer_country": "US",
    "payer_zip": "43000",
    "payer_ip": "192.168.0.1",
    "term_url_3ds": "https://example.com/3ds-callback",
    "recurring_init": "N",
    "auth": "N"
   }
   
6. Pay attention:
   ```shell
   After each sent request, the "order_id" value must be changed
