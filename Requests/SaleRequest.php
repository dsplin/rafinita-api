<?php

class SaleRequest
{

    /**
     * Validates the order data.
     *
     * @param array $orderData
     * @return array
     */
    public static function validate($orderData)
    {
        $errors = [];

        if (empty($orderData['action'])) {
            $errors[] = 'Action is required.';
        } elseif (!in_array($orderData['action'], ['SALE', 'CAPTURE', 'CREDITVOID'])) {
            $errors[] = 'Invalid action.';
        }

        if (empty($orderData['order_id'])) {
            $errors[] = 'Order ID is required.';
        }

        if (empty($orderData['order_amount'])) {
            $errors[] = 'Order amount is required.';
        } elseif (!self::isValidNumeric($orderData['order_amount'])) {
            $errors[] = 'Invalid order amount.';
        }

        if (empty($orderData['order_currency'])) {
            $errors[] = 'Order currency is required.';
        } elseif (!self::isValidCurrency($orderData['order_currency'])) {
            $errors[] = 'Invalid order currency.';
        }

        if (empty($orderData['order_description'])) {
            $errors[] = 'Order description is required.';
        }

        if (empty($orderData['card_number'])) {
            $errors[] = 'Card number is required.';
        } elseif (!self::isValidCardNumber($orderData['card_number'])) {
            $errors[] = 'Invalid card number.';
        }

        if (empty($orderData['card_exp_month'])) {
            $errors[] = 'Card expiration month is required.';
        } elseif (!self::isValidMonth($orderData['card_exp_month'])) {
            $errors[] = 'Invalid card expiration month.';
        }

        if (empty($orderData['card_exp_year'])) {
            $errors[] = 'Card expiration year is required.';
        } elseif (!self::isValidYear($orderData['card_exp_year'])) {
            $errors[] = 'Invalid card expiration year.';
        }

        if (empty($orderData['card_cvv2'])) {
            $errors[] = 'CVV2 is required.';
        } elseif (!self::isValidCVV2($orderData['card_cvv2'])) {
            $errors[] = 'Invalid CVV2.';
        }

        if (empty($orderData['payer_first_name'])) {
            $errors[] = 'Payer first name is required.';
        }

        if (empty($orderData['payer_last_name'])) {
            $errors[] = 'Payer last name is required.';
        }

        if (!empty($orderData['payer_email']) && !self::isValidEmail($orderData['payer_email'])) {
            $errors[] = 'Invalid email address.';
        }

        if (empty($orderData['payer_phone'])) {
            $errors[] = 'Payer phone number is required.';
        }

        if (empty($orderData['payer_address'])) {
            $errors[] = 'Payer address is required.';
        }

        if (empty($orderData['payer_city'])) {
            $errors[] = 'Payer city is required.';
        }

        if (empty($orderData['payer_country'])) {
            $errors[] = 'Payer country is required.';
        }

        if (empty($orderData['payer_zip'])) {
            $errors[] = 'Payer ZIP code is required.';
        }

        if (empty($orderData['payer_ip'])) {
            $errors[] = 'Payer IP address is required.';
        } elseif (!self::isValidIPAddress($orderData['payer_ip'])) {
            $errors[] = 'Invalid payer IP address.';
        }

        if (empty($orderData['term_url_3ds'])) {
            $errors[] = '3DS term URL is required.';
        } elseif (!self::isValidURL($orderData['term_url_3ds'])) {
            $errors[] = 'Invalid 3DS term URL.';
        }

        if (empty($orderData['recurring_init'])) {
            $errors[] = 'Recurring init is required.';
        }

        if (empty($orderData['auth'])) {
            $errors[] = 'Auth is required.';
        }

        return $errors;
    }

    /**
     * Checks if a value is numeric.
     *
     * @param mixed $value
     * @return bool
     */
    private static function isValidNumeric($value)
    {
        return is_numeric($value);
    }

    /**
     * Checks if a currency is valid.
     *
     * @param string $currency
     * @return bool
     */
    private static function isValidCurrency($currency)
    {
        $allowedCurrencies = ['USD', 'EUR', 'GBP'];

        return in_array($currency, $allowedCurrencies);
    }

    /**
     * Checks if a card number is valid.
     *
     * @param string $cardNumber
     * @return bool
     */
    private static function isValidCardNumber($cardNumber)
    {
        return true;
    }

    /**
     * Checks if a month is valid.
     *
     * @param int $month
     * @return bool
     */
    private static function isValidMonth($month)
    {
        return is_numeric($month) && $month >= 1 && $month <= 12;
    }

    /**
     * Checks if a year is valid.
     *
     * @param int $year
     * @return bool
     */
    private static function isValidYear($year)
    {
        $currentYear = date('Y');
        return is_numeric($year) && $year >= $currentYear;
    }

    /**
     * Checks if a CVV2 is valid.
     *
     * @param string $cvv2
     * @return bool
     */
    private static function isValidCVV2($cvv2)
    {
        return true;
    }

    /**
     * Checks if an email address is valid.
     *
     * @param string $email
     * @return bool
     */
    private static function isValidEmail($email)
    {
        return true;
    }

    /**
     * Checks if an IP address is valid.
     *
     * @param string $ipAddress
     * @return bool
     */
    private static function isValidIPAddress($ipAddress)
    {
        return true;
    }

    /**
     * Checks if a URL is valid.
     *
     * @param string $url
     * @return bool
     */
    private static function isValidURL($url)
    {
        return true;
    }
}
